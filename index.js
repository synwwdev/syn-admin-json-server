const PORT = 4848
const faker = require('faker');
const jsonServer = require('json-server')
const server = jsonServer.create()
const path = require('path')
const router = jsonServer.router(path.join(__dirname, 'db.json'))
const middlewares = jsonServer.defaults()

server.use(middlewares)

// Add custom routes before JSON Server router
server.get('/echo', (req, res) => {
    res.jsonp(req.query)
})

server.use(jsonServer.bodyParser)
server.use((req, res, next) => {
    if (req.method === 'POST') {
        req.body = Object.assign({'id': faker.random.uuid()}, req.body)
    }
    if (req.method === 'PUT') {
        req.body = Object.assign({'id': req.params.id}, req.body)
    }
    if (req.method === 'POST' || req.method === 'PUT' || req.method === 'PATCH') {
        req.body.modificationDate = (new Date).toISOString()
    }
    // Continue to JSON Server router
    next()
})

server.use(jsonServer.rewriter({
    '/itemService/api/v1/promotions/:country/campaigns/shippingfees/:id': '/shippingfees/:id',
    '/itemService/api/v1/promotions/:country/campaigns/shippingfees': '/shippingfees',
    '/itemService/api/v1/promotions/:country/:resouce/:id': '/:resouce/:id',
    '/itemService/api/v1/promotions/:country/:resouce': '/:resouce',
}))

router.render = (req, res) => {
    if (req.originalUrl == '/shippingfees' && req.method === 'GET') {
        res.jsonp({
            "defaults": {
              "shippingFee": 2.5
            },
            "campaigns": res.locals.data
        });
    } else {
        res.jsonp(res.locals.data);
    }
}

server.use(router);

server.listen(PORT, () => {
  console.log(`JSON Server is running on Port ${PORT}`)
})